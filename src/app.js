class Deck{
  constructor(){
    this.deck = [];
    this.reset();
    this.shuflle();
    //this.x ;

  }
  reset(){
    this.deck = [];
    let suits = ["Hearts" , "Spades" , "Clubs" , "Diamonds"];
    let pips = ["Ace" , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 , "Jack" , "Queens" , "King"]

    for(let suit in suits){
      for(let pip in pips){
        let x = {
              pip1:  pips[pip],
              suit1: suits[suit]
        }
        this.deck.push(x)
      }
    }
  }

  shuflle(){
    let m = this.deck.length , t , i
    while(m){
      i = Math.floor(Math.random() * m--);
      t = this.deck[m];
      this.deck[m] = this.deck[i];
      this.deck[i] = t;
    }
    return this;
  };

  deal(){
    return this.deck.pop();

  }
  peek() {
  return this.deck.slice(-1)[0];
}
}

let deck1 = new Deck() ;

let suit2 = ['', '', '', '', ''];
let pip2 = ['', '', '', '', ''];
for (var i = 0; i < suit2.length; i++) {
  let y = deck1.deal();
  pip2[i] = y.pip1;
  suit2[i] = y.suit1;
  console.log(y);
  console.log(pip2[i]);
  console.log(suit2[i]);
}
console.log(deck1.deck.length);
